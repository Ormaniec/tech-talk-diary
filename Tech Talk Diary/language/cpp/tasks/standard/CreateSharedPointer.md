# Create simple shared pointer

## My answer
```cpp
#pragma once

namespace ormaniec
{
    template<class T>
    class shared_ptr
    {
    private:
        unsigned* m_instance_counter = nullptr;
        T* mp_data = nullptr;

    public:

        explicit shared_ptr(T* data)
        {
            m_instance_counter = new unsigned(1);
            mp_data = data;
        }
        shared_ptr(shared_ptr& source)
        {
            mp_data = source.mp_data;
            m_instance_counter = source.m_instance_counter;
            (*m_instance_counter)++;
        }
	
        ~shared_ptr()
        {
            if( *m_instance_counter == 1 )
            {
                delete m_instance_counter;
                delete mp_data;
            }
            (*m_instance_counter)--;
        }

        unsigned getCount() const
        {
            return *m_instance_counter;
        }
	
        T* get() const { return mp_data; }
        T& operator*() const { return *mp_data; }
    };
}
```
